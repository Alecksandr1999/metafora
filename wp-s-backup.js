document.addEventListener('DOMContentLoaded', function() {
  gsap.registerPlugin(ScrollTrigger);
  const mobile = window.matchMedia('(max-width: 480px)');
	if (mobile.matches) {
		document.body.classList.remove('sticky-toolbar-on');
	}
	
	let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
  
  window.addEventListener('resize', () => {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });

  /* -- LENIS -- */
  const lenis = new Lenis({
    syncTouch: true,
    touchInertiaMultiplier: 25,
    velocity: 2,
    duration: 2
  })
  
  function raf(time) {
    lenis.raf(time);
    requestAnimationFrame(raf);
  }
  
  lenis.on('scroll', ScrollTrigger.update);
  gsap.ticker.add(time => lenis.raf(time * 1000));
  gsap.ticker.lagSmoothing(0);
	lenis.stop();
	
  /* -- DEBOUNCER -- */
  function debounce(func) {
    let timer;
    return function(event) {
      if (timer) clearTimeout(timer);
      timer = setTimeout(func, 200, event);
    };
  }
	
	/* -- PRELOADER -- */
  const city = document.getElementById('city');

  function pageLoaded() {
    document.body.classList.add('loaded');
    animHero();
		lenis.start();
    city['auto-rotate'] = true;
  }

  city.addEventListener('load', pageLoaded);
	
  /* -- HERO SECTION -- */
	let isMobile = mobile.matches;
  const heroBackground = document.getElementById('hero-background');
  let image = document.getElementById('hero-background-image');

  function createVideo() {
    const source = document.createElement('source');
    source.src = 'https://meta4a.io/wp-content/uploads/2023/08/hero-background-1.mp4';
    source.type = 'video/mp4';
    const video = document.createElement('video');
    video.id ='hero-background-video';
    video.classList.add('hero_background-video');
    video.autoplay = true;
    video.muted = true;
    video.loop = true;
    video.preload = true;
    video.appendChild(source);
    return video;
  }

  if (!isMobile) { heroBackground.replaceChild(createVideo(), image); }
  
  let heroTl = gsap.timeline({ defaults: { ease: 'power1.easeInOut'} });

  function animHero() {
    const rollUp = document.querySelector('.rollUp'),
          rollDown = document.querySelector('.rollDown');
  
    rollUp.classList.add('animated');
    rollDown.classList.add('animated');
  
    heroTl.to('.hero .slideUp', {
      stagger: 0.2,
      'clip-path': 'polygon(0% 100%, 100% 100%, 100% 0%, 0% 0%)',
      opacity: 1,
      delay: 1,
      duration: 1.5,
      y: 0
    }).to('.about .slideUp', {
      stagger: 0.2,
      'clip-path': 'polygon(0% 100%, 100% 100%, 100% 0%, 0% 0%)',
      opacity: 1,
      y: 0,
      duration: 0.5
    }, '-=1.5');
	}

  gsap.set('.hero_bottom .slideUp', { y: 0 });
  const heroBottomTl = gsap.timeline({
    ease: 'none',
    scrollTrigger: {
      trigger: '.about',
      start: `-${Math.ceil(10 * vh)} center`,
      end: 'bottom center',
      scrub: false,
      toggleActions: 'play none none reverse',
    }
  });

  heroBottomTl.to('.hero_bottom .slideUp', {
    y: 100,
    'clip-path': 'polygon(0 100%, 100% 100%, 100% 100%, 0 100%)',
    opacity: 0,
    duration: 1
  });

  /* -- ABOUT SECTION -- */
  const aboutContainer = document.querySelector('.about');
  let rightCols, aboutTl;

  function initAbout() {
    rightCols = document.querySelectorAll('.about-panel--desktop');
    gsap.set('.about-panel--desktop', { zIndex: (i) => i + 1 });
    aboutTl = gsap.timeline({
      defaults: { ease: 'none', duration: 2 },
      scrollTrigger: {
        trigger: aboutContainer,
        start: () => 'top top',
        end: () => `+=${rightCols[0].clientHeight * rightCols.length}`,
        pin: aboutContainer,
        pinSpacing: true,
        scrub: true
      }
    });
  }

  function initAboutMobile() {
    rightCols = document.querySelectorAll('.about-panel--mob');
    gsap.set('.about-panel--mob', { zIndex: (i) => i + 1 });
    aboutTl = gsap.timeline({
      defaults: { ease: 'none', duration: 3 },
      scrollTrigger: {
        trigger: aboutContainer,
        start: () => 'top top',
        end: () => `+=${(100 * vh) * (rightCols.length * 2)}`,
        pin: aboutContainer,
        pinSpacing: true,
        scrub: true
      }
    });

    heroBottomTl.to('.about_top .slideDown', {
      y: 0,
      'clip-path': 'polygon(0 0, 100% 0, 100% 100%, 0 100%)',      
      opacity: 1,
      duration: 1
    });
  }

  mobile.matches ? initAboutMobile() : initAbout();

  rightCols.forEach(col => {
    aboutTl
    .from(col, {
      yPercent: 70,
      autoAlpha: 0
    })
    .to(col, {
      yPercent: 0,
      autoAlpha: 1
    })
    .to(col, {
      yPercent: -70,
      autoAlpha: 0
    })
  });

  /* -- CASES SECTION -- */
  const container = document.querySelector('.cases'),
        shapes = document.querySelectorAll('.cases_slide-shape');

  const slides = gsap.utils.toArray('.cases_slide:not(:last-child)');
  gsap.set('.cases_slide', { zIndex: (i) => i + 1 });

  let tl;

  function initMobileTl() {
    tl = gsap.timeline({
      defaults: { ease: 'none' },
      scrollTrigger: {
        trigger: container,
        start: () => 'top +=20',
        end: () => `+=${slides[0].clientHeight * (slides.length * 2)}`,
        pin: container,
        pinSpacing: true,
        scrub: true
      }
    });
  }

  function initTl() {
    tl = gsap.timeline({
      defaults: { ease: 'none' },
      scrollTrigger: {
        trigger: container,
        start: () => 'top top',
        end: () => `+=${slides[0].clientWidth * slides.length}`,
        pin: container,
        pinSpacing: true,
        scrub: true
      }
    });
  }

  mobile.matches ? initMobileTl() : initTl();

  shapes.forEach(shape => {
    tl.to(shape, {
      duration: 8,
      stagger: 1,
      attr: {d:'M 1 -0.3 Q 1.5 0.5 1 1.3 L -0.5 1.3 L -0.5 -0.3 z'},
    });
  });

  let cityLoadingTimeout = setTimeout(() => city.dismissPoster(), 1000);
  /* -- THE END -- */
});